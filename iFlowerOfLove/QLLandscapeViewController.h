//
//  QLLandscapeViewController.h
//  iFlowerOfLove
//
//  Created by pascal on 19/04/14.
//  Copyright (c) 2014 2v-p.tv. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QLLandscapeViewController : UIViewController
{
    // Adds the Landscape image
    UIImage *lanscapeImage;
    
}
// Properties
@property (nonatomic,assign) id delegate;


// Init Interface
- (void) initInterface;
@end
