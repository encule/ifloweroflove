//
//  QLAppDelegate.h
//  iFlowerOfLove
//
//  Created by pascal on 16/04/14.
//  Copyright (c) 2014 2v-p.tv. All rights reserved.
//

#import <UIKit/UIKit.h>

// Interface
#import "QLLandscapeViewController.h"

@interface QLAppDelegate : UIResponder <UIApplicationDelegate>
{
    // --- Interface elements
    QLLandscapeViewController *landscapeViewController;

    
}

// Main window
@property (strong, nonatomic) UIWindow *window;

// Init App
- (void) initApp;

// Init Interface
- (void) initInterface;

@end
