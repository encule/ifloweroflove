//
//  QLLandscapeViewController.m
//  iFlowerOfLove
//
//  Created by pascal on 19/04/14.
//  Copyright (c) 2014 2v-p.tv. All rights reserved.
//

#import "QLLandscapeViewController.h"

#pragma mark -
#pragma mark --- Private Methods ---

@interface QLLandscapeViewController ()

@end

#pragma mark -

@implementation QLLandscapeViewController

#pragma mark -
#pragma mark --- Properties  ---

#pragma mark Properties
@synthesize delegate = _delegate;

#pragma mark -
#pragma mark ---  Init ---

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void) initInterface
{
    // --- self.view

}

#pragma mark -
#pragma mark ---  Overiden methods ---

- (void)viewDidLoad
{
    //
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Adds the Landscape image
    //lanscapeImage = [UIImage imageNamed:@"anyImageName"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark --- Delegated Methods ---



#pragma mark -
#pragma mark ---  Dealloc ---

- (void) dealloc
{
    // Release Graphics
    [lanscapeImage release];
    
    // super dealloc
    [super dealloc];
}

@end
