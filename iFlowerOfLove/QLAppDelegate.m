//
//  QLAppDelegate.m
//  iFlowerOfLove
//
//  Created by pascal on 16/04/14.
//  Copyright (c) 2014 2v-p.tv. All rights reserved.
//

#import "QLAppDelegate.h"


@implementation QLAppDelegate

#pragma mark -
#pragma mark --- Init ---

#pragma mark  Init

- (id)init
{
    self = [super init];
    if (self)
    {
        // Initialization code here.
        NSLog(@"[super init]");
        
    }
    
    return self;
}

#pragma mark  Init App
- (void) initApp
{
    // --- Init Interface
	NSLog(@"Launching - Init Interface");
    [self initInterface];
}

#pragma mark  Init Interface
- (void) initInterface
{
    // --- statusBar
    [[UIApplication sharedApplication] setStatusBarHidden: YES withAnimation: UIStatusBarAnimationNone];
    
    // -- Main Window
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor blackColor];
    [self.window makeKeyAndVisible];
    [self.window performSelector:@selector(makeKeyAndVisible) withObject: nil afterDelay: 5.f];
    
    // --- statusBar
    [[UIApplication sharedApplication] setStatusBarHidden: YES withAnimation: UIStatusBarAnimationNone];
    
    
    // --- QLLandscapeViewController
    
        // Init QLLandscapeViewController
        landscapeViewController = [[QLLandscapeViewController alloc] init];
    
        // Init Interface
        [landscapeViewController initInterface];
    
        // Set delegate
        [landscapeViewController setDelegate: self];
    

    // --- Set the window rootViewController
    self.window.rootViewController = landscapeViewController;
    
    // --- Finished - Init Interface
    NSLog(@"Finished - Init Interface");
    
}


#pragma mark -
#pragma mark --- UIApplicationDelegate Protocol Methods  ---


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // --- initApp
	NSLog(@"Launching - Init App");
    [self initApp];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark -
#pragma mark --- Dealloc ---

- (void)dealloc
{
    // Release landscapeViewController
    [landscapeViewController release];
    
    // Release_window
    [_window release];
    
    //
    [super dealloc];
}

@end
