//
//  main.m
//  iFlowerOfLove
//
//  Created by pascal on 16/04/14.
//  Copyright (c) 2014 2v-p.tv. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "QLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([QLAppDelegate class]));
    }
}
